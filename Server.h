//
// Created by wojtowic on 24.07.18.
//

#ifndef POCOHTTPTEST_SERVER_H
#define POCOHTTPTEST_SERVER_H


#include <Poco/Util/ServerApplication.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/ThreadPool.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Util/ServerApplication.h>

#include "RequestHandlerFactory.h"

class Server : public Poco::Util::ServerApplication{
public:
    void start() {
        Poco::ThreadPool pool(512, 512);
        Poco::Net::ServerSocket socket(4000, 64);
        Poco::Net::HTTPServer s(new RequestHandlerFactory(), pool, socket, new Poco::Net::HTTPServerParams());
        s.start();
        // wait for CTRL-C or kill
        Poco::Util::ServerApplication::waitForTerminationRequest();
        // Stop the HTTPServer
        s.stop();
    }
};


#endif //POCOHTTPTEST_SERVER_H

//
// Created by wojtowic on 24.07.18.
//

#ifndef POCOHTTPTEST_REQUESTHANDLER_H
#define POCOHTTPTEST_REQUESTHANDLER_H


#include <Poco/Net/HTTPRequestHandler.h>

class RequestHandler : public Poco::Net::HTTPRequestHandler {
public:
    void handleRequest(Poco::Net::HTTPServerRequest & request, Poco::Net::HTTPServerResponse & response) final;
};


#endif //POCOHTTPTEST_REQUESTHANDLER_H

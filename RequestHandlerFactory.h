//
// Created by wojtowic on 24.07.18.
//

#ifndef POCOHTTPTEST_REQUESTHANDLERFACTORY_H
#define POCOHTTPTEST_REQUESTHANDLERFACTORY_H

#include <Poco/Net/HTTPRequestHandlerFactory.h>


class RequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {

public:
    Poco::Net::HTTPRequestHandler *createRequestHandler(const Poco::Net::HTTPServerRequest &request) final;
};


#endif //POCOHTTPTEST_REQUESTHANDLERFACTORY_H

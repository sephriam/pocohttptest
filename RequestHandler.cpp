//
// Created by wojtowic on 24.07.18.
//

#include "RequestHandler.h"
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/URI.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/StreamCopier.h>
#include <memory>
#include <queue>
#include <mutex>

std::queue<std::shared_ptr<Poco::Net::HTTPClientSession>> sessionQueue;
std::mutex sessionQueueMux;

void RequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {

    std::string path("/");
    Poco::URI uri("http://localhost:80/");

    std::shared_ptr<Poco::Net::HTTPClientSession> session;
    {
        std::unique_lock<std::mutex> l(sessionQueueMux);
        if(sessionQueue.empty()) {
            session = std::make_unique<Poco::Net::HTTPClientSession>(uri.getHost(), uri.getPort());
            session->setKeepAlive(true);
            Poco::Timespan timespan(59, 0);
            session->setTimeout(timespan, timespan, timespan);
        } else {
            session = std::move(sessionQueue.front());
            sessionQueue.pop();
        }
    }

    Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_GET, path, Poco::Net::HTTPMessage::HTTP_1_1);

    Poco::Net::HTTPResponse res;
    session->sendRequest(req);
    std::istream& rs = session->receiveResponse(res);
    response.setStatus(Poco::Net::HTTPServerResponse::HTTPStatus::HTTP_OK);
    response.setContentLength(res.getContentLength());
    Poco::StreamCopier::copyStream(rs, response.send());

    {
        std::unique_lock<std::mutex> l(sessionQueueMux);
        sessionQueue.push(std::move(session));
    }
}

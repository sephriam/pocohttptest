//
// Created by wojtowic on 24.07.18.
//

#include "RequestHandlerFactory.h"
#include "RequestHandler.h"

Poco::Net::HTTPRequestHandler *
RequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest &request) {
    return new RequestHandler();
}
